# _WAAW09_ invoice-pdf

毎週1つWebアプリを作ろうの9回目。

期間は18/07/06 - 18/07/12

PDFを自在に操れるようになるのが最終的なゴールやけど、まずは簡単に請求書を作れるようになること。


## ゴール

* [ ] pdfで請求書が作れる
* [ ] pdfを自由に扱えるようになる

## 目的

* [ ] pdfがどういったデータを持って作られているかを理解する
* [ ] 請求書のpdfを簡単に作れるようにする

## 課題

* [x] pdf生成ライブラリ
    * [mikeshimura/goreport: Golang Pdf Report Generator](https://github.com/mikeshimura/goreport)
    * [unidoc/unidoc: UniDoc is a PDF library for Golang](https://github.com/unidoc/unidoc)
    * [jung-kurt/gofpdf: A PDF document generator with high level support for text, drawing and images](https://github.com/jung-kurt/gofpdf)
    * [signintech/gopdf: A simple library for generating PDF written in Go lang](https://github.com/signintech/gopdf)
    
        gofpdfを使うことに決定。goreportはうまく動かなかった。
            
        gofpdfはutf-8に対応してなかった。gopdfを試すことにする。

* [x] 請求書フォーマット
    * [請求書エクセルテンプレート(無料)](https://template.the-board.jp/invoice_templates)
    * [請求書エクセルテンプレート(無料)_タテ型_繰越金額_値引き_源泉徴収_008](https://template.the-board.jp/invoice_templates/article/invoice_template_008)

* [x] フォント
    * gofpdfで自由にフォントを使おうとすると、makefontでjsonファイルを作っておく必要があるみたい。
    * そもそもgofpdfはutf-8に対応してない。
    * gopdfでフォントを自由に指定でき、上手く動いた

* [x] pdfにテキストの追加
* [x] pdfに罫線の追加
* [x] テキストの大きさや太さを変更
* [x] left, center, right
* [x] top, bottom, middle
* [ ] margin, padding
* [ ] 罫線の太さや色を変更
* [ ] 塗りつぶし

## 振り返り

0. Idea:

    アイデアやテーマについて
    * 今回は請求書やけど、納品書でもなんでも自動でpdfがでると嬉しい場面は多い
    * officeのソフト等は仕様が変わるが、pdfは変わらないので安心して使える

0. What went right:

    成功したこと・できたこと
    * pdfの操作
    * 文字の追加や削除

0. What went wrong:

    失敗したこと・できなかったこと
    * margin, padding
    * 複雑な表組
    * 色塗り

0. What I learned:

    学べたこと
    * pdfのフォーマットがどういったものなのか

## サンプル

なし

package main

import (
	"github.com/signintech/gopdf"
	"log"
	"time"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// locationの設定
	const location = "Asia/Tokyo"
	loc, err := time.LoadLocation(location)
	if err != nil {
		loc = time.FixedZone(location, 9*60*60)
	}
	time.Local = loc
}

func main() {
	p := new(myPdf).init()
	p.addFont("migmix", "./ttf/migmix-1m-regular.ttf", true)

	//タイトル
	p.setFontSize(32)
	p.addCellInText(1, 1, p.cellNum, 3, "請求書", gopdf.CellOption{
		Align:  gopdf.Center | gopdf.Middle,
		Border: gopdf.Bottom,
		Float:  gopdf.Right,
	})
	p.addBr()

	//請求No
	p.setFontSize(16)
	p.addCellInText(10, 4, 3, 1, "請求No", gopdf.CellOption{
		Align:  gopdf.Right | gopdf.Middle,
		Border: 0,
		Float:  gopdf.Right,
	})
	p.addCellInText(13, 4, 3, 1, "af58b07cf9", gopdf.CellOption{
		Align:  gopdf.Right | gopdf.Middle,
		Border: 0,
		Float:  gopdf.Right,
	})
	p.addBr()

	//請求日
	p.setFontSize(16)
	p.addCellInText(10, 5, 3, 1, "請求日", gopdf.CellOption{
		Align:  gopdf.Right | gopdf.Middle,
		Border: 0,
		Float:  gopdf.Right,
	})
	p.addCellInText(13, 5, 3, 1, "2018/07/10", gopdf.CellOption{
		Align:  gopdf.Right | gopdf.Middle,
		Border: 0,
		Float:  gopdf.Right,
	})
	p.addBr()

	//宛先
	p.addCellInText(2, 6, p.cellNum-5, 1, "テストExample株式会社 御中", p.defOpt)
	p.addBr()
	p.addCellInText(2, 7, p.cellNum-5, 1, "ご担当者: 山田鈴木 様", p.defOpt)
	p.addBr()

	//請求額
	p.addCellInText(2, 9, p.cellNum-5, 1, "下記の通り、ご請求申し上げます。", p.defOpt)
	p.addBr()

	p.setFontSize(24)
	p.addCellInText(2, 10, 3, 2, "合計金額: ", gopdf.CellOption{
		Align:  gopdf.Center | gopdf.Middle,
		Border: gopdf.Bottom,
		Float:  gopdf.Right,
	})
	p.addCellInText(5, 10, 3, 2, "￥10,000", gopdf.CellOption{
		Align:  gopdf.Right | gopdf.Middle,
		Border: gopdf.Bottom,
		Float:  gopdf.Right,
	})
	p.addCellInText(8, 10, 3, 2, "(税別)", gopdf.CellOption{
		Align:  gopdf.Center | gopdf.Middle,
		Border: gopdf.Bottom,
		Float:  gopdf.Right,
	})
	p.addBr()

	//期限
	//振込先
	//請求者
	//内訳
	//備考

	p.write("invoice.pdf")
}

type myPdf struct {
	pdf        gopdf.GoPdf
	pageX      float64
	pageY      float64
	marginLeft float64
	lineHeight float64
	cellNum    int
	font       string
	fontSize   int
	cellHeight float64
	defOpt     gopdf.CellOption
}

func (p *myPdf) init() *myPdf {
	p.pdf = gopdf.GoPdf{}
	p.pageX = 595.28
	p.pageY = 841.89
	p.lineHeight = 1.25
	p.cellNum = 16
	p.fontSize = 16
	p.cellHeight = float64(p.fontSize) * p.lineHeight
	p.marginLeft = 10

	p.pdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: p.pageX, H: p.pageY}})
	p.addPage()
	p.pdf.SetLeftMargin(p.marginLeft)

	p.defOpt = gopdf.CellOption{
		Align:  gopdf.Left | gopdf.Middle,
		Border: 0,
		Float:  gopdf.Right,
	}

	return p
}

func (p *myPdf) addPage() {
	p.pdf.AddPage()
}

func (p *myPdf) addFont(name, path string, isSet bool) {
	if err := p.pdf.AddTTFFont(name, path); err != nil {
		log.Fatalln(err)
		return
	}

	if isSet {
		p.setFont(name)
	}
}

func (p *myPdf) setFont(name string) {
	p.font = name
	if err := p.pdf.SetFont(p.font, "", p.fontSize); err != nil {
		log.Fatalln(err)
		return
	}
}

func (p *myPdf) setFontSize(size int) {
	p.fontSize = size
	p.setFont(p.font)
}

func (p *myPdf) addBr() {
	p.pdf.Br(float64(p.fontSize) * p.lineHeight)
}

func (p *myPdf) addCellInText(cellX, cellY, mergeX, mergeY int, text string, opt gopdf.CellOption) {
	width := p.pageX
	cellWidth := width / float64(p.cellNum)
	p.pdf.SetX(cellWidth * float64(cellX-1))
	p.pdf.SetY(p.cellHeight * float64(cellY-1))

	rect := gopdf.Rect{
		W: cellWidth * float64(mergeX),
		H: p.cellHeight * float64(mergeY),
	}
	//log.Println(p.pageX, p.pageY, p.pdf.GetX(), p.pdf.GetY(), rect, p.pdf.GetX()+rect.W, p.pdf.GetY()+rect.H)

	if err := p.pdf.CellWithOption(&rect, text, opt); err != nil {
		log.Fatalln(err)
	}
}

func (p *myPdf) write(name string) {
	p.pdf.WritePdf(name)
}
